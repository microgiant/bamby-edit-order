<?php
/**
 * MicroGiantGroup
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MicroGiantGroup.com license that is
 * available through the world-wide-web at this URL:
 * http://www.microgiantgroup.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category    MicroGiantGroup
 * @package     MicroGiantGroup_Editorder
 * @copyright   Copyright (c) 2012 MicroGiantGroup (http://www.microgiantgroup.com/)
 * @license     http://www.microgiantgroup.com/license-agreement.html
 */

/**
 * Editorder Adminhtml Controller
 * 
 * @category    MicroGiantGroup
 * @package     MicroGiantGroup_Editorder
 * @author      MicroGiantGroup Developer
 */
class MicroGiant_Editorder_Adminhtml_EditorderController extends Mage_Adminhtml_Controller_Action
{
    /**
     * init layout and set active for current menu
     *
     * @return MicroGiant_Editorder_Adminhtml_EditorderController
     */
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('editorder/editorder')
            ->_addBreadcrumb(
                Mage::helper('adminhtml')->__('Items Manager'),
                Mage::helper('adminhtml')->__('Item Manager')
            );
        return $this;
    }
 
    /**
     * index action
     */
    public function indexAction()
    {
        $this->_initAction();
		  $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
            $this->_addContent($this->getLayout()->createBlock('editorder/adminhtml_editorder_edit'));

            $this->renderLayout();
    }

    /**
     * view and edit item action
     */
    public function editAction()
    {
        $editorderId     = $this->getRequest()->getParam('id');
        $model  = Mage::getModel('editorder/editorder')->load($editorderId);

        if ($model->getId() || $editorderId == 0) {
            $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
            if (!empty($data)) {
                $model->setData($data);
            }
            Mage::register('editorder_data', $model);

            $this->loadLayout();
            $this->_setActiveMenu('editorder/editorder');

            $this->_addBreadcrumb(
                Mage::helper('adminhtml')->__('Item Manager'),
                Mage::helper('adminhtml')->__('Item Manager')
            );
            $this->_addBreadcrumb(
                Mage::helper('adminhtml')->__('Item News'),
                Mage::helper('adminhtml')->__('Item News')
            );

            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
            $this->_addContent($this->getLayout()->createBlock('editorder/adminhtml_editorder_edit'))
                ->_addLeft($this->getLayout()->createBlock('editorder/adminhtml_editorder_edit_tabs'));

            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('editorder')->__('Item does not exist')
            );
            $this->_redirect('*/*/');
        }
    }
 
    public function newAction()
    {
        $this->_forward('edit');
    }
 
    /**
     * save item action
     */
    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) {
			$increment_id=$this->getRequest()->getParam('increment_id');
			$order=Mage::getModel('sales/order')->loadByIncrementId($increment_id);
			if($order && $order->getId()){
				$customer = Mage::getModel('customer/customer');
				$customer->setWebsiteId(Mage::app()->getWebsite()->getId()?Mage::app()->getWebsite()->getId():1);
				$customer=$customer->loadByEmail($order->getCustomerEmail());
				if($customer&&$customer->getId()){
				$order->setCustomerGroupId($customer->getGroupId());
				$order->setCustomerId($customer->getId());
				try{
					$order->save();
					Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('editorder')->__('Order #%s have been updated sucessfully!',$increment_id));
				}
				catch(Exception $ex){
					 Mage::getSingleton('adminhtml/session')->addError(
					 Mage::helper('editorder')->__('Cannot save order')
					);
				}
			}				
			}
			else  Mage::getSingleton('adminhtml/session')->addError(
					 Mage::helper('editorder')->__('Order #%s does not exist',$increment_id)
					);
			
        }
       
        $this->_redirect('*/*/');
    }
 
    public function loadOrderInfoAction()
	{
		$increment_id=$this->getRequest()->getParam('increment_id');
		$order=Mage::getModel('sales/order')->loadByIncrementId($increment_id);
		if($order&&$order->getId()){
			$order->setCustomerGroupName(Mage::helper('editorder')->getCustomerGroupName($order->getCustomerGroupId()));
			echo Mage::helper('core')->jsonEncode($order->getData());
		}
		else echo '';
		return;
	}
    
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('editorder');
    }
}