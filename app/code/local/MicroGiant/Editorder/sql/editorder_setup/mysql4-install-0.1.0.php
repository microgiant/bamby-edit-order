<?php
/**
 * MicroGiantGroup
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MicroGiantGroup.com license that is
 * available through the world-wide-web at this URL:
 * http://www.microgiantgroup.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category    MicroGiantGroup
 * @package     MicroGiantGroup_Editorder
 * @copyright   Copyright (c) 2012 MicroGiantGroup (http://www.microgiantgroup.com/)
 * @license     http://www.microgiantgroup.com/license-agreement.html
 */

/** @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

/**
 * create editorder table
 */
$installer->endSetup();

