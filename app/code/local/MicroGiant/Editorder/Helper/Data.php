<?php
/**
 * MicroGiantGroup
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MicroGiantGroup.com license that is
 * available through the world-wide-web at this URL:
 * http://www.microgiantgroup.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category    MicroGiantGroup
 * @package     MicroGiantGroup_Editorder
 * @copyright   Copyright (c) 2012 MicroGiantGroup (http://www.microgiantgroup.com/)
 * @license     http://www.microgiantgroup.com/license-agreement.html
 */

/**
 * Editorder Helper
 * 
 * @category    MicroGiantGroup
 * @package     MicroGiantGroup_Editorder
 * @author      MicroGiantGroup Developer
 */
class MicroGiant_Editorder_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function getCustomerGroupName($groupID)
    {
            return Mage::getModel('customer/group')->load($groupID)->getCode();
    }
}