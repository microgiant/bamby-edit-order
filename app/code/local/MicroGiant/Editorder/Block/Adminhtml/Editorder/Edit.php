<?php
/**
 * MicroGiantGroup
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MicroGiantGroup.com license that is
 * available through the world-wide-web at this URL:
 * http://www.microgiantgroup.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category    MicroGiantGroup
 * @package     MicroGiantGroup_Editorder
 * @copyright   Copyright (c) 2012 MicroGiantGroup (http://www.microgiantgroup.com/)
 * @license     http://www.microgiantgroup.com/license-agreement.html
 */

/**
 * Editorder Edit Block
 * 
 * @category     MicroGiantGroup
 * @package     MicroGiantGroup_Editorder
 * @author      MicroGiantGroup Developer
 */
class MicroGiant_Editorder_Block_Adminhtml_Editorder_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
        
        $this->_objectId = 'id';
        $this->_blockGroup = 'editorder';
        $this->_controller = 'adminhtml_editorder';
        $this->_updateButton('save', 'label', Mage::helper('editorder')->__('Update Order'));
        $this->_removeButton('delete', 'label', Mage::helper('editorder')->__('Delete Item'));
        $this->_formScripts[] = "

            $('edit_form').onsubmit = function () {
               return false;
           };

            function toggleEditor() {
                if (tinyMCE.getInstanceById('editorder_content') == null)
                    tinyMCE.execCommand('mceAddControl', false, 'editorder_content');
                else
                    tinyMCE.execCommand('mceRemoveControl', false, 'editorder_content');
            }
			function loadOrderInfo(el){
				var increment_id=el.value;
				new Ajax.Request('"
                . $this->getUrl('*/*/loadOrderInfo', array('_current' => true))
                . "', {
                            parameters: {
                                         form_key: FORM_KEY,
                                         increment_id: increment_id,
                                         },
                            evalScripts: true,
                            onSuccess: function(transport) {
                                if(transport.responseText){
								  var order =JSON.parse(transport.responseText);
								  $('order_info').show();
                                  $('order_id').update(order.entity_id);
								  $('customer_email').update(order.customer_email);
                                  $('customer_group_name').update(order.customer_group_name);
								  $('customer_name').update(order.customer_firstname + ' ' + order.customer_lastname);
                                }
                                else{
    								alert('".$this->__('Not found')."');
                                    $('order_info').hide();
                                }
                            }
                        });
			}
        ";
    }
    
    /**
     * get text to show in header when edit an item
     *
     * @return string
     */
    public function getHeaderText()
    {
       
        return ;
    }
}