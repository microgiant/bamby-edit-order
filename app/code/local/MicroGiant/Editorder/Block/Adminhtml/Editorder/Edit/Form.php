<?php
/**
 * MicroGiantGroup
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MicroGiantGroup.com license that is
 * available through the world-wide-web at this URL:
 * http://www.microgiantgroup.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category     MicroGiantGroup
 * @package     MicroGiantGroup_Editorder
 * @copyright     Copyright (c) 2012 MicroGiantGroup (http://www.microgiantgroup.com/)
 * @license     http://www.microgiantgroup.com/license-agreement.html
 */

/**
 * Editorder Edit Form Block
 * 
 * @category    MicroGiantGroup
 * @package     MicroGiantGroup_Editorder
 * @author      MicroGiantGroup Developer
 */
class MicroGiant_Editorder_Block_Adminhtml_Editorder_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * prepare form's information for block
     *
     * @return MicroGiant_Editorder_Block_Adminhtml_Editorder_Edit_Form
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form(array(
            'id'        => 'edit_form',
            'action'    => $this->getUrl('*/*/save', array(
                'id'    => $this->getRequest()->getParam('id'),
            )),
            'method'    => 'post',
            'enctype'   => 'multipart/form-data'
        ));

         $fieldset = $form->addFieldset('editorder_form', array(
            'legend'=>Mage::helper('editorder')->__('Edit Order')
        ));

        $fieldset->addField('increment_id', 'text', array(
            'label'        => Mage::helper('editorder')->__('Increment ID'),
            'class'        => 'required-entry',
            'required'    => true,
            'name'        => 'increment_id',
			'after_element_html'  =>'
            </tr><tr><td style="font-weight: bold;" colspan="2" class="label" id="order_infor">
			<div id="order_info" style="border: solid 1px #3d6611;display:none;">
			<div class="lengend" style="margin: 10px 10px;font-size: 16px;">'.$this->__('Order Information: ').'</div>
			<div class="content">
				<table cellspacing="0" class="form-list">
                <tbody>
				<tr>
                    <td class="label"><label>'.$this->__('Order Id').'</label></td>
                    <td class="value">
                        <strong id="order_id"></strong>
                    </td>
                </tr>
				
                <tr>
                    <td class="label"><label>'.$this->__('Email').'</label></td>
                    <td class="value"><a href="#mail"><strong id="customer_email"></strong></a></td>
                </tr>

                <tr>
                    <td class="label"><label>'.$this->__('Name').'</label></td>
                    <td class="value"><strong id="customer_name"></strong></td>
                </tr>
                <tr>
                    <td class="label"><label>Customer Group</label></td>
                    <td class="value"><strong id="customer_group_name"></strong></td>
                </tr>
				</tbody></table>
			</div>
			</div>
			</td>
			</tr>


            ',
			'onchange'=>'loadOrderInfo(this)',
			
        ));



        $form->setUseContainer(true);
        $this->setForm($form);
        return parent::_prepareForm();
    }
}