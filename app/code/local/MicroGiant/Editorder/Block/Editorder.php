<?php
/**
 * MicroGiantGroup
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MicroGiantGroup.com license that is
 * available through the world-wide-web at this URL:
 * http://www.microgiantgroup.com/license-agreement.html
 * 
 * DISCLAIMER  asda
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category    MicroGiantGroup
 * @package     MicroGiantGroup_Editorder
 * @copyright   Copyright (c) 2012 MicroGiantGroup (http://www.microgiantgroup.com/)
 * @license     http://www.microgiantgroup.com/license-agreement.html
 */

/**
 * Editorder Block adasdasd
 * 
 * @category    MicroGiantGroup
 * @package     MicroGiantGroup_Editorder
 * @author      MicroGiantGroup Developer
 */
class MicroGiant_Editorder_Block_Editorder extends Mage_Core_Block_Template
{
    /**
     * prepare block's layout
     *
     * @return MicroGiant_Editorder_Block_Editorder
     */
    public function _prepareLayout()
    {
        return parent::_prepareLayout();
    }
}